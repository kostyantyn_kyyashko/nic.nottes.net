<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Controller;

use App\Core\Rabbit\RMonitor;
use App\Core\Shop;
use App\Core\Rabbit\RabbitClient;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RmonitorController
 * @package App\Controller
 */
class ApiController extends Controller
{
    /**
     * @Route("/api/workerCount")
     * @return Response
     */
    public function workerCount(Request $request)
    {
        $rm = new RMonitor();
        $worker = $request->query->get('worker', 'ads:worker');
        $worker = urldecode($worker);
        $count = $rm::workerCount($worker);
        return new Response($count);
    }

    /**
     * @Route("/api/workerStart")
     * @param Request $request
     * @return Response
     */
    public function workerStart(Request $request)
    {
        $rm = new RMonitor();
        $worker = $request->query->get('worker', 'ads:worker');
        $worker = urldecode($worker);
        $rm::workerStart($worker);
        return new Response('ok');
    }

    /**
     * @Route("/api/workerStop")
     * @param Request $request
     * @return Response
     */
    public function workerStop(Request $request)
    {
        $rm = new RMonitor();
        $worker = $request->query->get('worker', 'ads:worker');
        $worker = urldecode($worker);
        $rm::workerStop($worker);
        return new Response('ok');
    }

    /**
     * @Route("/api/createTasks")
     * @param Request $request
     * @return Response
     */
    public function createTasks(Request $request)
    {
        Shop::$shopUrl = 'https://santech-control.ru/y-market/saved_file.xml';
        Shop::$delta = 3;
        try {
            $shop = new Shop();
            $data = $shop->getNewRemovedChangedOfferIds();
        }
        catch (\Throwable $exception) {
            echo $exception->getMessage();
        }
        $client = new RabbitClient();
        $client->addServer();
        $counter = [
            'createNewAds' => 0,
            'removeAds' => 0,
            'changePriceAds' => 0,
            'stopAds' => 0,
        ];
        foreach ($data['newOffersIds'] as $newOfferId) {
            $rabbitData = [
                'handler' => '\App\Core\Handler3::createNewAds',
                'data' => $shop->allXmlOffers[$newOfferId],
            ];
            $client->doBackground('work', serialize($rabbitData));
            $counter['createNewAds']++;
        }
        foreach ($data['removedOfferIds'] as $removedOfferId) {
            $rabbitData = [
                'handler' => '\App\Core\Handler3::removeAds',
                'data' => $shop->allMongoOffers[$removedOfferId],
            ];
            $client->doBackground('work', serialize($rabbitData));
            $counter['removeAds']++;

        }
        foreach ($data['changePriceIds'] as $changePriceId) {
            $rabbitData = [
                'handler' => '\App\Core\Handler3::changePriceAds',
                'data' => $shop->allXmlOffers[$changePriceId],
            ];
            $client->doBackground('work', serialize($rabbitData));
            $counter['changePriceAds']++;
        }
        foreach ($data['changeAvailableIds'] as $changeAvailableId) {
            $rabbitData = [
                'handler' => '\App\Core\Handler3::stopAds',
                'data' => $shop->allXmlOffers[$changeAvailableId],
            ];
            $client->doBackground('work', serialize($rabbitData));
            $counter['stopAds']++;
        }

        $shop->removeOffersFromMongo();
        $shop->insertOffersToMongo($shop->allXmlOffers);

        return $this->json($counter);
    }


    /**
     * @Route("/api/status")
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function queuesStatus()
    {
        return $this->json(RMonitor::fullQueuesStatus());
    }

}
