<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Controller;

use App\Core\GMonitor;
use App\Core\Parser;
use App\Core\Parsers\Amazon\SellersDataParser;
use App\Core\Rabbit\RMonitor;
use App\Core\Shop;
use App\Document\Items\Amazon\BigData\SellersData;
use App\Document\Items\Proxy;
use App\Document\Model\Offers;
use App\Document\MongoBase;
use App\Document\MongoManager;
use App\Document\Utils;
use Doctrine\MongoDB\Tests\RetryTest;
use MongoDB\Driver\Manager;
use MongoDB\Driver\Server;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RmonitorController
 * @package App\Controller
 */
class RmonitorController extends Controller
{
    /**
     * @Route("/rmonitor")
     * @return Response
     */
    public function mainPage ()
    {
        return $this->render('rmonitor/init.html.twig');
    }

    /**
     * @Route("/t18")
     */
    public function t18show()
    {
        $rm = new RMonitor();
        $resp = $rm::removeFunctionQueue('work');
        Utils::debugView($resp);
        return new Response('');
    }

    /**
     * @Route("/t18r")
     */
    public function remove()
    {
        $shop = new Shop('https://santech-control.ru/y-market/saved_file.xml');
        $shop->removeOffersFromMongo();
        return new Response('');

    }


}
