<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Core;

use App\Core\Rabbit\RabbitWorker;
use PhpAmqpLib\Message\AMQPMessage;

/**
 * Array $data as argument of all functions has offer's structure
 * ['offerId'=>...,'categoryId'=>...,'available'=>...,'price'=>...,'name'=>...,'vendor'=>...,'timestamp'=>...,]
 * all values has int type
 * Class Handler3
 * @package App\Core
 */
class Handler3
{
    /**
     * @param AMQPMessage $msg
     * @param array $data - see structure above
     */
    public static function createNewAds(AMQPMessage $msg, array $data)
    {
        echo "new ads " . $data['name'] . "\n";
        RabbitWorker::sendAck($msg);
    }

    /**
     * @param AMQPMessage $msg
     * @param array $data - see structure above
     */
    public static function changePriceAds(AMQPMessage $msg, array $data)
    {
        echo "change price item " . $data['name'] . "\n";
        RabbitWorker::sendAck($msg);
    }

    /**
     * @param AMQPMessage $msg
     * @param array $data - see structure above
     */
    public static function removeAds(AMQPMessage $msg, array $data)
    {
        echo "remove ads for item " . $data['name'] . "\n";
        RabbitWorker::sendAck($msg);
    }

    /**
     * @param AMQPMessage $msg
     * @param array $data - see structure above
     */
    public static function stopAds(AMQPMessage $msg, array $data)
    {
        echo "stop ads for item " . $data['name'] . "\n";
        RabbitWorker::sendAck($msg);
    }


}
