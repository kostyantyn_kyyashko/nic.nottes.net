<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Core;

use App\Document\Model\Offers;
use App\Document\MongoManager;

class Shop
{
    /**
     * The allowable price difference at which the price is not considered new.
     * It is necessary for small fluctuations in exchange rates
     * IN PERSENT
     * @var float
     */
    public static $delta = 2;

    /**
     * XML URL
     * Attention! must be defined before call "new Shop()"
     * @var
     */
    public static $shopUrl;

    /**
     * @var string
     */
    private $url;

    /**
     * @var array
     */
    public $allXmlOffers = [];

    /**
     * @var array
     */
    public $allMongoOffers = [];

    /**
     * @var array
     */
    private $xmlOffers = [];

    /**
     * @var array
     */
    private $mongoOffers = [];

    /**
     * Shop constructor.
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function __construct()
    {
        $this->url = static::$shopUrl;
        $this->getMongoOffers();
        $this->getXmlOffers();
    }

    /**
     * get XML from $this->url
     * @return mixed
     */
    private function getXml()
    {
        return self::httpSourceGet($this->url)['content'];
        return $xml;
    }
    /**
    * get full data of web page or xml file
    * @param $url
    * @return mixed
    */
    private function httpSourceGet($url){
        $options = [
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => true,     //return headers in addition to content
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 3,      // timeout on connect
            CURLOPT_TIMEOUT        => 3,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
            CURLINFO_HEADER_OUT    => true,
            CURLOPT_SSL_VERIFYPEER => false,     // Disabled SSL Cert checks
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,

        ];


        $ch = curl_init( $url );
        curl_setopt_array( $ch, $options );
        $rough_content = curl_exec( $ch );
        $err     = curl_errno( $ch );
        $errmsg  = curl_error( $ch );
        $header  = curl_getinfo( $ch );
        curl_close( $ch );

        $header_content = substr($rough_content, 0, $header['header_size']);
        $body_content = trim(str_replace($header_content, '', $rough_content));
        $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
        preg_match_all($pattern, $header_content, $matches);
        $cookies_out = implode("; ", $matches['cookie']);

        $header['errno']   = $err;
        $header['errmsg']  = $errmsg;
        $header['headers']  = $header_content;
        $header['content'] = $body_content;
        $header['cookies'] = $cookies_out;
        return $header;
    }

    /**
     * create offer's array from xml
     * @return array
     */
    public function getXmlOffers()
    {
        $xml = $this->getXml();
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);
        $array = json_decode($json,1);
        $out = [];
        foreach ($array['shop']['offers']['offer'] as $offer) {
            $offerId = intval($offer['@attributes']['id']);
            $out[$offerId]['offerId'] = intval($offer['@attributes']['id']);
            $out[$offerId]['categoryId'] = intval($offer['categoryId']);
            $out[$offerId]['available'] = ($offer['@attributes']['available'] == 'true')?1:0;
            $out[$offerId]['price'] = intval($offer['price']);
            $out[$offerId]['name'] = $offer['name'];
            $out[$offerId]['vendor'] = $offer['vendor'];
            $out[$offerId]['timestamp'] = time();
        }
        $this->allXmlOffers = $out;
        $this->xmlOffers = $out;
        return $array['shop']['offers']['offer'];
    }

    /**
     * insert Offers to mongo
     * @param array $offers
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function insertOffersToMongo(array $offers)
    {
        foreach ($offers as $offerid => $offer) {
            $builder = MongoManager::getInstance()->createManager()->createQueryBuilder('\App\Document\Model\Offers');
            foreach ($offer as $field => $value) {
                $builder
                    ->insert()
                    ->field($field)->set($value);
            }
            $builder
                ->getQuery()
                ->execute();
        }
    }

    /**
     * insert Offers to mongo
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function removeOffersFromMongo()
    {
        $builder = MongoManager::getInstance()->createManager()->createQueryBuilder('\App\Document\Model\Offers');
        $builder
            ->remove()
            ->getQuery()
            ->execute();
    }

    /**
     * select all offers from Mongo
     * @return array
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    private function getMongoOffers()
    {
        $builder = MongoManager::getInstance()->createManager()->createQueryBuilder('\App\Document\Model\Offers');
        $offers = $builder
            ->hydrate(false)
            ->getQuery()
            ->execute()
            ->toArray();
        $out = [];
        foreach ($offers as $offer){
            unset($offer['_id']);
            $out[$offer['offerId']] = $offer;
        }
        $this->allMongoOffers = $out;
        $this->mongoOffers = $out;
        return $out;
    }

    /**
     * get offer ids from offers array
     * @param array $offers
     * @return array
     */
    private function getOfferIds(array $offers)
    {
        return array_keys($offers);
    }

    public function getNewRemovedChangedOfferIds()
    {

        $xmlOfferIds = $this->getOfferIds($this->xmlOffers);
        $mongoOfferIds = $this->getOfferIds($this->mongoOffers);

        $newOfferIds = array_diff($xmlOfferIds, $mongoOfferIds);
        $removedOfferIds = array_diff($mongoOfferIds, $xmlOfferIds);

        foreach ($newOfferIds as $id) {
            unset($xmlOfferIds[$id]);
        }
        foreach ($removedOfferIds as $id) {
            unset($mongoOfferIds[$id]);
        }

        $changePriceIds = [];
        $changeAvailableIds = [];
        foreach ($this->xmlOffers as $newOffer) {
            if (array_key_exists($newOffer['offerId'], $this->mongoOffers)) {
                $oldOffer = $this->mongoOffers[$newOffer['offerId']]; //echo ($oldOffer['price']) . "\n"; die();
                if (abs(($newOffer['price'] - $oldOffer['price'])/$oldOffer['price']*100) > static::$delta) {
                    $changePriceIds[] = $newOffer['offerId'];
                }
                if ($newOffer['available'] != $oldOffer['available']) {
                    $changeAvailableIds[] = $newOffer['offerId'];
                }
            }
        }

        return [
            'newOffersIds' => $newOfferIds,
            'removedOfferIds' => $removedOfferIds,
            'changePriceIds' => $changePriceIds,
            'changeAvailableIds' => $changeAvailableIds,
        ];
    }

}
