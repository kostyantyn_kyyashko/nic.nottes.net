<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Core\Rabbit;

use App\Document\Utils;

class RMonitor
{
    const HOST = 'localhost';
    const PORT = 5672;
    const USER = 'konst20';//set your <username>, see readme.md
    const PASS = 'rbzirj25';//set your <password>, see readme.md
    const HTTP_HOST = 'http://nic.nottes.net:15672';//set your rabbitmMQ web host, see readme.md
    const ROOT_DIR = '/var/www/nic.nottes.net';//root directory of project



    /**
     * @param string $worker
     * @return int
     */
    public static function workerCount($worker)
    {
        $command_string = 'ps ax | grep ' . $worker;
        ob_start();
        system($command_string);
        $raw_out = ob_get_contents();
        ob_clean();
        return count(explode("\n", $raw_out)) - 3;
    }

    /**
     * @param string $worker - 'main' or 'fake'
     * @return string
     */
    private static function workerCommandString($worker)
    {
        return "php bin/console $worker ";
    }

    /**
     * Start worker
     * @param string $worker
     */
    public static function workerStart($worker){
        exec("cd " . static::ROOT_DIR . ";" . static::workerCommandString($worker) ." > /dev/null &");
    }

    /**
     * Stop workers
     * @param string $worker
     */
    public static function workerStop($worker){
        $worker = '"' . $worker . '"';
        exec("ps ax | grep $worker | awk '{print $1}' | xargs kill");
    }

    /**
     * get full data of web page, include header, coockies etc
     * @param $url string
     * @param $mode string
     * @param $data array
     * @return mixed
     */
    private static function webPageGet($url, $mode = 'GET', $data = null){
        $userPass = self::USER . ':' . self::PASS;
        $headers = [
            'Content-Type:application/json',
            'Authorization: Basic '. base64_encode("{$userPass}")
        ];
        $options = [
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => true,     //return headers in addition to content
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 3,      // timeout on connect
            CURLOPT_TIMEOUT        => 3,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
            CURLINFO_HEADER_OUT    => true,
            CURLOPT_SSL_VERIFYPEER => false,     // Disabled SSL Cert checks
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_HTTPHEADER     => $headers,

        ];

        if ($mode != 'GET') {
            switch ($mode){
                case 'DELETE':
                    $options[CURLOPT_CUSTOMREQUEST] = 'DELETE';
                    $options[CURLOPT_POSTFIELDS] = $data;
                    break;
                case 'POST':
                    $options[CURLOPT_POST] = 1;
                    $options[CURLOPT_POSTFIELDS] = http_build_query($data);
                    break;
            }

        }

        $ch = curl_init( $url );
        curl_setopt_array( $ch, $options );
        $rough_content = curl_exec( $ch );
        $err     = curl_errno( $ch );
        $errmsg  = curl_error( $ch );
        $header  = curl_getinfo( $ch );
        curl_close( $ch );
        
        $header_content = substr($rough_content, 0, $header['header_size']);
        $body_content = trim(str_replace($header_content, '', $rough_content));
        $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
        preg_match_all($pattern, $header_content, $matches);
        $cookies_out = implode("; ", $matches['cookie']);

        $header['errno']   = $err;
        $header['errmsg']  = $errmsg;
        $header['headers']  = $header_content;
        $header['content'] = $body_content;
        $header['cookies'] = $cookies_out;
        return $header['content'];
    }

    /**
     * @param string $function - function's name
     */
    public static function resetFunctionQueue($function)
    {
        $data = [
            'vhost' => '/',
            'mode' => 'purge',
            'name' => $function,
        ];
//        $function = urlencode($function);
        $url = static::HTTP_HOST . "/api/queues/%2F/{$function}/contents";
        return self::webPageGet($url, 'DELETE', json_encode($data));
    }

    /**
     * @param string $function - function's name
     */
    public static function removeFunctionQueue($function)
    {
        $data = [
            'vhost' => '/',
            'mode' => 'delete',
            'name' => $function,
        ];
        $function = urlencode($function);
        $url = static::HTTP_HOST . "/api/queues/%2F/{$function}";
        return self::webPageGet($url, 'DELETE', json_encode($data));
    }

    /**
     * all queues status
     * @return array
     */
    public static function fullQueuesStatus()
    {
        $queues = self::webPageGet(static::HTTP_HOST . "/api/queues");
        $queues = json_decode($queues, 1); //return $queues;
        $status = [];
        if ($queues && count($queues) > 0) {
            foreach ($queues as $queue) {
                try {
                    $o['function_name'] = $queue['name'];
                    $o['waiting'] = $queue['messages_ready'];
                    $o['in_progress'] = $queue['messages_unacknowledged'];
                    $o['workers'] = $queue['consumers'];
                    $status[$queue['name']] = $o;
                }
                catch (\Throwable $e) {
                    echo $e->getMessage();
                }
            }
        }
        return $status;
    }

}
