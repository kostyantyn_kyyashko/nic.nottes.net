<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Core\Rabbit;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Message\AMQPMessage;

class RabbitWorker
{

    const USER = 'guest';

    const PASS = 'guest';

    /**
     * @var AMQPChannel
     */
    private $channel = null;

    /**
     * @var null|AMQPStreamConnection
     */
    private $connection = null;


    /**
     * gearman's analog
     * @param string $host
     * @param int $port
     */
    public function addServer(string $host = 'localhost', int $port = 5672)
    {
        $this->connection = new AMQPStreamConnection($host, $port, static::USER, static::PASS);
        $this->channel = $this->connection->channel();
    }

    /**
     * RabbitWorker constructor.
     */
    public function __construct()
    {
    }

    /**
     * Gearman's analog
     *
     * var $callback must be static method of class, or callable function
     * you can define full method name with namespace, f.e
     * '\App\Handlers\HandlerExample::test'
     * After call $callback() gets argument AMQPMessage $msg, f.e.\App\Handlers\HandlerExample::test(AMQPMessage $msg)
     * Data can be get by $msg->getBody()
     *
     * Attention! callback must send ACK, see below static method sednAck(), f.e.
     * function callback(AMQPMessage $msg) {...; RabbitWorker::sendAck($msg)}
     *
     * @todo durable true
     * @param string $function
     * @param string $callback
     */
    public function addFunction (string $function, string $callback)
    {
        $this->channel->queue_declare($function, false, false, false, true);
        $this->channel->basic_qos(null, 1, null);
        $this->channel->basic_consume($function, '', false,
                false, false, false, $callback);
    }

    /**
     * this method must be called in the end of callback function
     * @param AMQPMessage $msg
     */
    public static function sendAck (AMQPMessage $msg)
    {
        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
    }

    /**
     * imitation method GearmanWorker::work()
     * @return bool
     */
    public function work ()
    {
        while (count($this->channel->callbacks)) {
            $this->channel->wait();
        }

        return false;
    }
}
