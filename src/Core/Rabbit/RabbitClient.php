<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Core\Rabbit;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use \PhpAmqpLib\Channel\AMQPChannel;

class RabbitClient
{
    /**
     * Rabbit user. maybe need to create user on server
     */
    const USER = 'guest';

    /**
     * Rabbit password
     */
    const PASS = 'guest';

    /**
     * @var null|AMQPChannel
     */
    public $channel = null;

    /**
     * @var null|AMQPStreamConnection
     */
    private $connection = null;

    /**
     * RabbitClient constructor.
     */
    public function __construct()
    {
    }

    /**
     * Gearman's method analog
     * @param string $host
     * @param int $port
     */
    public function addServer(string $host = 'localhost', int $port = 5672)
    {
        $this->connection = new AMQPStreamConnection($host, $port, static::USER, static::PASS);
        $this->channel = $this->connection->channel();
    }


    /**
     * $function ia a queue's name.
     * !Attention $function() must send ACK
     * @todo durable true
     * @param $function
     */
    public function doBackground ($function, $data)
    {
        if ($this->connection && $this->channel) {
            $this->channel->queue_declare($function, false, false, false, true);
            $msg = new AMQPMessage($data, ['delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT]);
            $this->channel->basic_publish($msg, '', $function);
        }
    }

    public function close ()
    {
        $this->channel->close();
        $this->connection->close();
    }
}
