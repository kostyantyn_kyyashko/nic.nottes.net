<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Document\Model;

use App\Document\MongoBase;
use App\Document\MongoManager;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use MongoDB\Driver\Manager;

/**
 * Class Offer
 * @package App\Document\Model
 * @MongoDB\Document(db="shop")
 * @MongoDB\Indexes({
 *     @MongoDB\Index(keys={"offerId"="asc"}, unique=true),
 *     @MongoDB\Index(keys={"offerId"="asc", "categoryId"="asc"}),
 *     @MongoDB\Index(keys={"price"="asc"}),
 *     @MongoDB\Index(keys={"name"="asc"}),
 *     @MongoDB\Index(keys={"vendor"="asc"}),
 *     @MongoDB\Index(keys={"offerId"="asc", "price"="asc"}, unique=true),
 *     })

 */
class Offers extends MongoBase
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @MongoDB\Id
     * @var string
     */
    protected $id;

    /**
     * @MongoDB\Field(type="int")
     * @var int
     */
    protected $offerId;

    /**
     * @MongoDB\Field(type="int")
     * @var int
     */
    protected $categoryId;

    /**
     * @MongoDB\Field(type="int")
     * @var int
     */
    protected $available;

    /**
     * @MongoDB\Field(type="int")
     * @var int
     */
    protected $price;

    /**
     * @MongoDB\Field(type="int")
     * @var int
     */
    protected $timestamp;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $name;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    protected $vendor;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getOfferId(): int
    {
        return $this->offerId;
    }

    /**
     * @param int $offerId
     */
    public function setOfferId(int $offerId): void
    {
        $this->offerId = $offerId;
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return $this->categoryId;
    }

    /**
     * @param int $categoryId
     */
    public function setCategoryId(int $categoryId): void
    {
        $this->categoryId = $categoryId;
    }

    /**
     * @return int
     */
    public function getAvailable(): int
    {
        return $this->available;
    }

    /**
     * @param int $available
     */
    public function setAvailable(int $available): void
    {
        $this->available = $available;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice(int $price): void
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getTimestamp(): int
    {
        return $this->timestamp;
    }

    /**
     * @param int $timestamp
     */
    public function setTimestamp(int $timestamp): void
    {
        $this->timestamp = $timestamp;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getVendor(): string
    {
        return $this->vendor;
    }

    /**
     * @param string $vendor
     */
    public function setVendor(string $vendor): void
    {
        $this->vendor = $vendor;
    }



}
