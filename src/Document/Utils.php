<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Document;
use Symfony\Component\HttpFoundation\Request;

/**
 * small utils
 * Class Utils
 * @package App\Document
 */
class Utils
{

    /**
     * @param object $object
     * @param array $fieldValues
     */
    public static function setOjectRow (object &$object, array $fieldValues)
    {
        foreach ($fieldValues as $field => $value)
        {
            $class = get_class($object);
            if (property_exists($class, $field)) {
                $setter = 'set' . self::mb_ucfirst($field);
                if (method_exists($object, $setter))
                $object->$setter();
            }
        }
    }

    /**
     * first letter to UPPERCASE for utf8
     * @param $string
     * @param string $encoding
     * @return string
     */

    public static function mb_ucfirst($string, $encoding = 'utf-8')
    {
        $strlen = mb_strlen($string, $encoding);
        $firstChar = mb_substr($string, 0, 1, $encoding);
        $then = mb_substr($string, 1, $strlen - 1, $encoding);
        return mb_strtoupper($firstChar, $encoding) . $then;
    }

    /**
     * App root directory
     * @return string
     */
    public static function getRootDir ()
    {
        global $kernel;
        return $kernel->getContainer()->get('kernel')->getRootDir() . '/../';
    }

    /**
     * class name without NS
     * @param $classNameWithNs
     * @return mixed
     */
    public static function clearClassName ($classNameWithNs)
    {
        $class = explode('\\', $classNameWithNs);
        return end($class);
    }

    /**
     * @param $var
     */
    public static function debugView ($var, $die = false)
    {
        echo '<pre>';
        print_r($var);
        if ($die) die();
    }

    /**
     * @param $var
     */
    public static function debugDump ($var, $die = false)
    {
        echo '<pre>';
        var_dump($var);
        if ($die) die();
    }


    /**
     * load CPU in %
     * http://php.net/manual/ru/function.sys-getloadavg.php
     * @return float
     */
    public static function getServerLoad ($precision = 0)
    {
        $load = null;

        if (stristr(PHP_OS, "win"))
        {
            $cmd = "wmic cpu get loadpercentage /all";
            @exec($cmd, $output);

            if ($output)
            {
                foreach ($output as $line)
                {
                    if ($line && preg_match("/^[0-9]+\$/", $line))
                    {
                        $load = $line;
                        break;
                    }
                }
            }
        }
        else
        {
            if (is_readable("/proc/stat"))
            {
                // Collect 2 samples - each with 1 second period
                // See: https://de.wikipedia.org/wiki/Load#Der_Load_Average_auf_Unix-Systemen
                $statData1 = self::_getServerLoadLinuxData();
                sleep(1);
                $statData2 = self::_getServerLoadLinuxData();

                if
                (
                    (!is_null($statData1)) &&
                    (!is_null($statData2))
                )
                {
                    // Get difference
                    $statData2[0] -= $statData1[0];
                    $statData2[1] -= $statData1[1];
                    $statData2[2] -= $statData1[2];
                    $statData2[3] -= $statData1[3];

                    // Sum up the 4 values for User, Nice, System and Idle and calculate
                    // the percentage of idle time (which is part of the 4 values!)
                    $cpuTime = $statData2[0] + $statData2[1] + $statData2[2] + $statData2[3];

                    // Invert percentage to get CPU time, not idle time
                    $load = 100 - ($statData2[3] * 100 / $cpuTime);
                }
            }
        }

        return round($load, $precision);
    }

    private static function  _getServerLoadLinuxData()
    {
        if (is_readable("/proc/stat"))
        {
            $stats = @file_get_contents("/proc/stat");

            if ($stats !== false)
            {
                // Remove double spaces to make it easier to extract values with explode()
                $stats = preg_replace("/[[:blank:]]+/", " ", $stats);

                // Separate lines
                $stats = str_replace(array("\r\n", "\n\r", "\r"), "\n", $stats);
                $stats = explode("\n", $stats);

                // Separate values and find line for main CPU load
                foreach ($stats as $statLine)
                {
                    $statLineData = explode(" ", trim($statLine));

                    // Found!
                    if
                    (
                        (count($statLineData) >= 5) &&
                        ($statLineData[0] == "cpu")
                    )
                    {
                        return array(
                            $statLineData[1],
                            $statLineData[2],
                            $statLineData[3],
                            $statLineData[4],
                        );
                    }
                }
            }
        }

        return null;
    }




}
