<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Command;

use App\Core\Rabbit\RabbitClient;
use App\Core\Shop;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * This worker can be run by command "php bin/console <worker name>
 * The name of worker sets by configure(<worker name>) method
 * Class WorkerExample
 * @package App\Command
 */
class AdsCreateTasks extends Command
{

    protected function configure()
    {
        $this->setName('ads:createTasks');
        $this->setDescription('Create tasks and send it to queue');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        Shop::$shopUrl = 'https://santech-control.ru/y-market/saved_file.xml';
        $shop = new Shop();
        $data = $shop->getNewRemovedChangedOfferIds();
        $client = new RabbitClient();
        $client->addServer();
        foreach ($data['newOffersIds'] as $newOfferId) {
            $rabbitData = [
                'handler' => '\App\Core\Handler3::createNewAds',
                'data' => $shop->allXmlOffers[$newOfferId],
            ];
            $client->doBackground('work', serialize($rabbitData));
        }
        foreach ($data['removedOfferIds'] as $removedOfferId) {
            $rabbitData = [
                'handler' => '\App\Core\Handler3::removeAds',
                'data' => $shop->allMongoOffers[$removedOfferId],
            ];
            $client->doBackground('work', serialize($rabbitData));

        }
        foreach ($data['changePriceIds'] as $changePriceId) {
            $rabbitData = [
                'handler' => '\App\Core\Handler3::changePriceAds',
                'data' => $shop->allXmlOffers[$changePriceId],
            ];
            $client->doBackground('work', serialize($rabbitData));
        }
        foreach ($data['changeAvailableIds'] as $changeAvailableId) {
            $rabbitData = [
                'handler' => '\App\Core\Handler3::stopAds',
                'data' => $shop->allXmlOffers[$changeAvailableId],
            ];
            $client->doBackground('work', serialize($rabbitData));
        }

        $shop->removeOffersFromMongo();
        $shop->insertOffersToMongo($shop->allXmlOffers);
    }
}
