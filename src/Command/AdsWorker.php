<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
namespace App\Command;

use App\Core\Rabbit\RabbitWorker;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * This worker can be run by command "php bin/console <worker name>
 * The name of worker sets by configure(<worker name>) method
 * Class WorkerExample
 * @package App\Command
 */
class AdsWorker extends Command
{
    protected function configure()
    {
        $this->setName('ads:worker');//name of worker for run this worker from command string
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $worker = new RabbitWorker();
        $worker->addServer();
        $worker->addFunction('work', '\App\Command\AdsWorker::callback');
        while ($worker->work());
    }

    public function callback(AMQPMessage $msg)
    {
        $msgData = unserialize($msg->getBody());
        $handler = $msgData['handler'];
        $data = $msgData['data'];
        $handler($msg, $data);
    }
}
