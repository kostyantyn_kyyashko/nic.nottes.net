var updateInterval = 2000;


function renderFuctionTable()
{
    var tpl =_.template($('#functions_table_template').html());
    $('#functions_table_wrap').html(tpl());
    setInterval(function () {
        $.ajax({
            url: '/ajax/all_functions_statuses',
            dataType: 'json',
            success: function (resp) {
                var tpl2 = _.template($('#function_rows').html());
                if (resp.result === 'ok') {
                    $('#functions_tbody').html(tpl2({rdata: resp.data}));
                }
                resetFunctionQueue();
            }
        })
    }, updateInterval)
}

var options = '';
function workersList () {
    $.ajax({
        url: '/ajax/workers_list',
        type: 'get',
        success: function (resp) {
            $.each(JSON.parse(resp), function(index, name) {
                options += '<option value="' + name + '">' + name + '</option>'
            });
            $('select.workers_list').html(options);
        }
    })
}

function worker_add()
{
    $('#worker_add').click(function () {
        $.ajax({
            url: '/ajax/worker_start/',
            type: 'get',
            data: {
                count: $('input#worker_start_count').val(),
                worker: $('select#worker_start_name').val()
            },
            success: function (resp) {
                if (resp === 'ok') {
                    $.alert('Worker(s) Added');
                }
                else {
                    console.log(resp);
                    $.alert('Some Error Occured, view to browser console');
                }
            }
        })
    })
}

function workerSetCount()
{
    $('#worker_set_count').click(function () {
        var workerCount = $('input#worker_set_count_val').val();
        var worker = $('select#worker_set_count_name').val();
        $.ajax({
            url: '/ajax/worker_stop/?worker=' + worker,
            success: function (resp) {
                if (resp === 'ok') {
                    $.ajax({
                        url: '/ajax/worker_start',
                        type: 'get',
                        data: {
                            count: workerCount,
                            worker: worker
                        },
                        success: function (resp) {
                            if (resp === 'ok') {
                                $.alert('Count of Workers Set to ' + workerCount);
                            }
                            else {
                                console.log(resp);
                                $.alert('Worker Start Error, view to browser console');
                            }
                        }
                    })
                }
                else {
                    console.log(resp);
                    $.alert('Worker Stop Error, view to browser console');
                }
            }
        })
    })
}

function stopAllWorkers()
{
    $.ajax({
        url: '/ajax/stop_all_workers',
        success: function (resp) {
            if (resp === 'ok') {
                $.alert('All Workers Stopped');
            }
            else {
                console.log(resp);
                $.alert('Some Error Occured, view to browser console');
            }
        }
    })
}

function workerStopButtonBind() {
    $('#worker_stop').click(function () {
        $.ajax({
            url: '/ajax/worker_stop?worker=' + $('select#worker_stop_name').val(),
            success: function (resp) {
                if (resp === 'ok') {
                    $.alert('All Workers Stopped');
                }
                else {
                    console.log(resp);
                    $.alert('Some Error Occured, view to browser console');
                }
            }
        })
    });
}

function resetFunctionQueue()
{
    $('.reset')
        .unbind()
        .click(function () {
            var function_name = $(this).attr('data-function');
            $.ajax({
                url: '/ajax/reset_function_queue',
                data: {
                    function_name: function_name
                },
                success: function (resp) {
                    if (resp === 'ok') {
                        $.alert('Queue of ' + function_name + ' reset');
                    }
                    else {
                        if(resp === 'empty') {
                            $.alert('Queue of "' + function_name + '" function is EMPTY, reset not need');
                        }
                        else {
                            console.log(resp);
                            $.alert('Some Error Occured, view to browser console');
                        }
                    }
                }
            });
    })
}

function resetAllQueue()
{
    $.ajax({
        url: '/ajax/reset_all_queue',
        success: function (resp) {
            if (resp === 'ok') {
                $.alert('All Functions Queue Reset');
            }
            else {
                console.log(resp);
                $.alert('Some Error Occured, view to browser console');
            }
        }
    })
}

function resetAllQueueButtonBind() {
    $('#reset_all_functions').click(function () {
        resetAllQueue();
    })
}

function totalResetButtonBind () {
    $('#total_reset').click(function () {
        resetAllQueue();
        stopAllWorkers();
    })
}

$(document).ready(function () {
    renderFuctionTable();
    worker_add();
    workerSetCount();
    workerStopButtonBind();
    resetAllQueueButtonBind();
    totalResetButtonBind();
    workersList();
});